from django.shortcuts import render, redirect
from accounts.forms import LoginForm, SignUpForm
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User

# Create your views here.
def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            usern = form.cleaned_data["username"]
            passw = form.cleaned_data["password"]
            user = authenticate(request, username=usern, password=passw)
            if user is not None:
                login(request, user)
                return redirect("home")
            else:
                context = {"form": form}
                return render(request, "accounts/login.html", context)
    else:
        form = LoginForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def logout_view(request):
    logout(request)
    return redirect("login")

def signup_view(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            if form.cleaned_data["password"] == form.cleaned_data["password_confirmation"]:
                User.objects.create_user( username=form.cleaned_data["username"], password=form.cleaned_data["password"])
                return redirect("home")
            else:
                form.add_error("password", "the passwords do not match")
    else:
        form = SignUpForm()
        context = {
            "form":form,
        }
    return render(request, "accounts/signup.html",context)
