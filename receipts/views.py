from django.shortcuts import render,redirect
from receipts.models import Account, ExpenseCategory, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm

@login_required
def receipts(request):
    receipts_list = Receipt.objects.filter(purchaser = request.user)
    context = {"receipts": receipts_list}
    return render(request, "receipts/home.html", context)

@login_required
def create_receipts(request):
    if request.method == "POST":
        # form = ReceiptForm(request.user, request.POST)
        form = ReceiptForm(request.POST)
        form.fields["account"].queryset=Account.objects.filter(owner=request.user)
        form.fields["category"].queryset=ExpenseCategory.objects.filter(owner=request.user)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
        form.fields["account"].queryset=Account.objects.filter(owner=request.user)
        form.fields["category"].queryset=ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "form":form,
    }
    return render(request, "receipts/create_receipt.html",context)

@login_required
def expense_category_view(request):
    category_list = ExpenseCategory.objects.filter(owner = request.user)
    context = {"categories": category_list}
    return render(request, "receipts/categories.html", context)

@login_required
def account_list_view(request):
    account_list = Account.objects.filter(owner = request.user)
    context = {"accounts": account_list}
    return render(request, "receipts/accounts.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CategoryForm()
    context = {
        "form":form,
    }
    return render(request, "receipts/create_category.html",context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form":form,
    }
    return render(request, "receipts/create_account.html",context)
